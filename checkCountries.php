<?php
/*

Copyright 2016 International Center for Agricultural Research in the Dry Areas(ICARDA)
 
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
require './libs/ShapeFile.inc.php';
include 'classCoordinates.php';
include 'mysql_connection.php';
include 'find_column.php';

//check command arguments-- argv[1] is input, argv[2] is output
if ($argv[1] != "landorsea" && $argv[1] != "country") {
	echo "Please type input file name, and output file name :\n";
    echo "Example :php checkCountries.php country|landorsea input.csv output.csv";
    exit;
}
//check command arguments-- argv[1] is input, argv[2] is output
if ($argv[2] == "" || $argv[3] == "") {
    echo "Please type input file name, and output file name :\n";
    echo "Example :php checkCountries.php country|landorsea input.csv output.csv";
    exit;
}

$target_file = $argv[2];
//create result file
$resultfilename = $argv[3];

$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
$imageFileType2 = pathinfo($resultfilename, PATHINFO_EXTENSION);

// Allow only CSV file formats
if (($imageFileType != "csv") || ($imageFileType2 != "csv")) {
    echo "Sorry, only csv files are accepted \n";
    exit;
}

//looking for columns :
$col_SITE_CODE = find_column("SITE_CODE", $target_file);
$col_lat = find_column("LAT_DD", $target_file);
$col_lon = find_column("LON_DD", $target_file);

if($argv[1] == "country")
$col_CTY = find_column("CTY", $target_file);

//exit if columns not found :
if ($col_lat == -1 || $col_lon == -1) {
    echo "Couldn't find columns\n";
    exit;
}

//reading from the input file :
if (($fread = fopen($target_file, "r")) !== false) {
	//include googleapis
    echo '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>';

	//creating the output file :
    $f = fopen($resultfilename, "a");
	//writting the output file header :
	if($argv[1] == "country"){
    $results_header = "SITE_CODE, COUNTRY_CHECK, Country_IN_DB, LAT_DD, LON_DD, CORRECT_FOUND_COUNTRY";
	}
	else {
	$results_header = "SITE_CODE, Land_or_sea, LAT_DD, LON_DD";	
	}
    $results_header .= "\n";
    $arr = file($resultfilename);
    $arr[0] = $results_header;
	// write to file
    file_put_contents($resultfilename, implode($arr));

	//escape the input file header :
    $header = fgetcsv($fread, 1000, ",");
    while (($record = fgetcsv($fread, 1000, ",")) !== false) {
    //escaping null coordinates :
        if (($record[$col_lat] != "" && $record[$col_lon] != "") && ($record[$col_lat] != "0" || $record[$col_lon] != "0")) {
            
		    //Create an object:
            $coordinates_country = new Coordinates();
			//insert record in object :
            $coordinates_country->setLatitude($record[$col_lat]);
            $coordinates_country->setlongitude($record[$col_lon]);
			$coordinates_country->setSiteCode($record[$col_SITE_CODE]);
						
            echo "\nChecking :".$record[$col_SITE_CODE]." lat :".$record[$col_lat]." Lon :".$record[$col_lon]."\n";
            
			if($argv[1] == "landorsea"){
				if($coordinates_country->verify())
				{
					echo "Found \n";
					$current=$record[$col_SITE_CODE].','."Sea".",".$record[$col_lat].','.$record[$col_lon]."\n";
					file_put_contents($resultfilename, utf8_encode($current), FILE_APPEND);
				}
				else {
					$current=$record[$col_SITE_CODE].','."Land".",".$record[$col_lat].','.$record[$col_lon]."\n";
					file_put_contents($resultfilename, utf8_encode($current), FILE_APPEND);
				}
			
			}
			
			else 
			{
			$coordinates_country->setCountryIsoCode($record[$col_CTY]);
			//set the country name from the database, using the 3 iso code
            $coordinates_country->setCountry($connect);

			//send data to google maps API :
            $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$coordinates_country->getLatitude(
                ).",".$coordinates_country->getLongitude()."&sensor=true";
			//get the JSON file result :
            $data = @file_get_contents($url);
            $jsondata = json_decode($data, true);
			//get google MAPS returned status (OK, ZERO_RESULTS, OVER_QUERY_LIMIT)
            $status = $jsondata['status'];
            if (is_array($jsondata) && $status == "OK") {
                $found = false;
                //fetching the country
                foreach ($jsondata["results"] as $result) {
                    foreach ($result["address_components"] as $address) {
                        if (in_array("country", $address["types"])) {
                            if($found)break;
                            $coordinates_country->setMapsCountry($address["long_name"]);

                            echo "Found country from maps ".$coordinates_country->getMapsCountry()."\n";
							//comparing country result and given country
                            if (strcasecmp(
                                    $coordinates_country->getCountryNameInDb(),
                                    $coordinates_country->getMapsCountry()
                                ) != 0)
                             {
							 //Write results to the output file :
                                file_put_contents(
                                    $resultfilename,
                                    $coordinates_country->getSiteCode(
                                    ).','."INCORRECT,".'"'.$coordinates_country->getCountryNameInDb(
                                    ).'"'.','.$coordinates_country->getLatitude(
                                    ).','.$coordinates_country->getLongitude().','.$coordinates_country->getMapsCountry(
                                    )."\n",
                                    FILE_APPEND
                                );
                             } else {
                                file_put_contents(
                                    $resultfilename,
                                    $coordinates_country->getSiteCode(
                                    ).','."CORRECT,".'"'.$coordinates_country->getCountryNameInDb(
                                    ).'"'.','.$coordinates_country->getLatitude(
                                    ).','.$coordinates_country->getLongitude().','.$coordinates_country->getMapsCountry(
                                    )."\n",
                                    FILE_APPEND
                                );
                             }

                            $found = true;
                            break;

                        }
                        if ($found) {
                            break;
                        }
                    }
                }
            } //if the country was not found :
			else {
                echo "Failed with status '$status'\n";
                if ($status == 'ZERO_RESULTS') {
                    file_put_contents(
                        $resultfilename,
                        $coordinates_country->getSiteCode(
                        ).','."NOT FOUND,".'"'.$coordinates_country->getCountryNameInDb(
                        ).'"'.','.$coordinates_country->getLatitude().','.$coordinates_country->getLongitude(
                        ).','.''."\n",
                        FILE_APPEND
                    );
                }

            }

        }
    }
}
}

?>