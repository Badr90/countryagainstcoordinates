**Description :**

This CLI tool was developed to validate and correct mismatches between country of origin and geographical coordinates and also to verify if geographical coordinates are within land or sea/water.

* Coordinates/country mismatches :

Using the google maps API, the coordinates are reverse geo-coded in order to extract the corresponding country name from google.

The country name is compared to the given country from input file.

* Verify if land or sea :

Download the water-polygons shapefile in WGS84 projection from:

http://openstreetmapdata.com/data/water-polygons

Create **data/** folder and move the downloaded file to it.

This shapefile is derived from OSM data, © OpenStreetMap contributors.

**Input file :**

Should be a CSV file holding at least the columns : 

* SITE_CODE: Code of the site.

* LAT_DD : Decimal latitude coordinate.

* LON_DD : Decimal longitude coordinate.

* CTY    : 3-ISO country code.

Remark : CTY column is not required for landorsea checking

**Output file :**

The generated output file contains the following columns :

* SITE_CODE : Code of the site. 

* LAT_DD : Decimal latitude coordinate.

* LON_DD : Decimal longitude coordinate. 	

For the country Checking additional columns are inserted: 

* COUNTRY_CHECK : may take three different values, **CORRECT**: if the country matches, **INCORRECT**: if different country name is found, **NOT FOUND** : if the coordinates couldn't be reverse geocoded.
                
* Country_IN_DB : Corresponding country name to the 3-ISO code from the database.

* CORRECT_FOUND_COUNTRY: The name of the correct country from google maps API.

For landorsea Checking : 

* Land_or_sea : **Land** : coordinates within land borders, **Sea** : coordinates within water borders.

**Limitation :**

For country checking :

Google maps API has a use limitation of 2500 queries per day.

**Running the tool :**

php checkCountries.php landorsea|country <input.csv> <output.csv>