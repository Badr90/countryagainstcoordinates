<?php
/*

Copyright 2016 International Center for Agricultural Research in the Dry Areas(ICARDA)
 
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
class Coordinates
{
	//class attributes :
    private $latitude;
    private $longitude;
    private $countryIsoCode;
    private $countryNameInDb;
    private $maps_country;
    private $site_code;

	//Get and Set functions :
    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function getCountryIsoCode()
    {
        return $this->countryIsoCode;
    }

    public function getCountryNameInDb()
    {
        return $this->countryNameInDb;
    }

    public function getMapsCountry()
    {
        return $this->maps_country;
    }

    public function getSiteCode()
    {
        return $this->site_code;
    }


    public function setLatitude($l)
    {
        $this->latitude = $l;
    }

    public function setLongitude($l)
    {
        $this->longitude = $l;
    }

    public function setCountryIsoCode($c)
    {
        $this->countryIsoCode = $c;
    }

    public function setCountryNameInDb($c)
    {
        $this->countryNameInDb = $c;
    }

    public function setMapsCountry($m)
    {
        $this->maps_country = $m;
    }

    public function setSiteCode($c)
    {
        $this->site_code = $c;
    }
	//set country name from the database
    public function setCountry($connect)
    {
        $ori = $this->getCountryIsoCode();
        $query = "SELECT CTY_NAME FROM countries WHERE ORI='$ori'";

        $result = mysqli_query($connect, $query);

        if (($row = $result->fetch_array()) !== null) {
            $this->setCountryNameInDb($row[0]);
            //echo "Country is $coordinates_country->countryNameInDb <br>";
        }
    }

public function is_in_polygon($points_polygon, $vertices_x, $vertices_y)
{
    $i = $j = $c = 0;
    for ($i = 0, $j = $points_polygon; $i < $points_polygon; $j = $i++) {
        if ((($vertices_y[$i] > $this->latitude != ($vertices_y[$j] > $this->latitude)) &&
            ($this->longitude< ($vertices_x[$j] - $vertices_x[$i]) * ($this->latitude - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]))
        )
		{
            $c = !$c;
        }
    }
    return $c;
}

public function verify(){
	$options = array('noparts' => false);
	$shp = new ShapeFile("data/water_polygons.shp", $options); // along this file the class will use file.shx and file.dbf

	while ($record = $shp->getNext()) {
		
		// read shape data
		$shp_data = $record->getShpData();

		if(isset($shp_data['parts'] ))
		foreach ($shp_data['parts'] as $part) 
		{
			$vertices_x = array();
			$vertices_y = array();		
			foreach ($part['points'] as $point) {
				//get the x coordinates of the country
				$vertices_x[]= $point['x'];
				//get the y coordinates of the country
				$vertices_y[]= $point['y'];					
			}
			
			$points_polygon = count($vertices_x) - 1;
			if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y)) 
			{
				echo  $this->site_code." is inside water polygons<br>";
				return true;
			}
		}	
	}
	return false;
}


}
?>